package com.cognizant.CMS.bean;

public class Permission {

	private boolean isView=false;
	private boolean isCreate=false;
	private boolean isModify=false;
	private boolean isDelete=false;

	public Permission(boolean isView, boolean isCreate, boolean isModify, boolean isDelete) {
		setView(isView);
		setCreate(isCreate);
		setModify(isModify);
		setDelete(isDelete);
	}

	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

	public boolean isCreate() {
		return isCreate;
	}

	public void setCreate(boolean isCreate) {
		this.isCreate = isCreate;
	}

	public boolean isModify() {
		return isModify;
	}

	public void setModify(boolean isModify) {
		this.isModify = isModify;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public String toString() {
		return "\n Permission [isView()=" + isView() + ", isCreate()=" + isCreate() + ", isModify()=" + isModify()
				+ ", isDelete()=" + isDelete() + "]";
	}

	public Permission() {
		super();
	}
	

}
