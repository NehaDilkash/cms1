package com.cognizant.CMS.bean;

public class User {

	private String userName;
	private String password;
	private Role roles;
	private String firstName;
	private String lastName;

	public User(String userName, String password, Role roles, String firstName, String lastName) {
		
		setUserName(userName);
		setPassword(password);
		setRoles(roles);
		setFirstName(firstName);
		setLastName(lastName);
	}

	public User() {
		super();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName){
		if(userName.equalsIgnoreCase("")||userName.equalsIgnoreCase(" "))
		{
			
			throw new RuntimeException("this field can't be empty");
			
		}
		else
		{
			this.userName = userName;
		}
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		if(password.equalsIgnoreCase("")||password.equalsIgnoreCase(" "))
		{
			
			throw new RuntimeException("this field can't be empty");
		}
		else if(isValid(password)==true)
		{
			this.password = password;
		}
		else
		{
			
			throw new RuntimeException("password critera does not match please reenter password");
		}
	}

	public Role getRoles() {
		return roles;
	}

	public void setRoles(Role roles) {
		this.roles = roles;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		if(firstName.equalsIgnoreCase("")||firstName.equalsIgnoreCase(" "))
		{
			System.out.println("this field can't be empty");
		}
		else
		{
			this.firstName = firstName;
		}
		
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		if(lastName.equalsIgnoreCase("")||lastName.equalsIgnoreCase(" "))
		{
			throw new RuntimeException("this field can't be empty");
		}
		else
		{
			this.lastName = lastName;
		}
	}

	 public static boolean isValid(String password) {  
	        //return true if and only if password:
	        //1. have at least eight characters.
	        //2. consists of only letters and digits.
	        //3. must contain at least two digits.
	        if (password.length() < 8) {   
	            return false;  
	        } else {      
	            char c;  
	            int count = 1;   
	            for (int i = 0; i < password.length() - 1; i++) {  
	                c = password.charAt(i);  
	                if (!Character.isLetterOrDigit(c)) {          
	                    return false;  
	                } else if (Character.isDigit(c)) {  
	                    count++;  
	                    if (count < 2)   {     
	                        return false;  
	                    }     
	                }  
	            }  
	        }  
	        return true;  
	    }

	@Override
	public String toString() {
		return "User [getUserName()=" + getUserName() + ", getPassword()=" + getPassword() + ", \n getRoles()="
				+ getRoles() + ", getFirstName()=" + getFirstName() + ", getLastName()=" + getLastName() + "]";
	}  
	 
}
